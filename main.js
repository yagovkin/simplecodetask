var cards_arr = [
    '4111111111111111',
    '4111111111111',
    '4012888888881881',
    '378282246310005',
    '6011111111111117',
    '5105105105105100',
    '5105 1051 0510 5106',
    '9111111111111111'
];

var card_types = ['AMEX', 'Discover', 'MasterCard', 'VISA', 'Unknown'];


init(cards_arr);


function init(cards) {
    // Array that holds JSON cards objects
    var cards_obj_arr = [];

    // Fill the array
    cards.forEach(function (card) {
        var card_number = card.replace(/\s/g, '');
        var card_type = getCardType(card_number);
        var card_obj = {number: card_number, card_type: card_type};
        // Set 'valid' property to 0 if card type is 'Unknown', otherwise call validateCard() function
        card_obj.valid = card_type == card_types[4] ? 0 : validateCard(card_obj);

        cards_obj_arr.push(card_obj);
    });

    printCards(cards_obj_arr);
}


function getCardType(card) {
    var card_type = card_types[4];

    if (card.substring(0, 2) == 34 || card.substring(0, 2) == 37) card_type = card_types[0];
    if (card.substring(0, 4) == 6011) card_type = card_types[1];
    if (card.substring(0, 2) > 50 && card.substring(0, 2) < 56) card_type = card_types[2];
    if (card.substring(0, 1) == 4) card_type = card_types[3];

    return card_type;
}


function validateCard(card) {

    var valid_length = 0;

    // Validate card number length
    switch (card.card_type) {
        case card_types[0]:
            if (card.number.length == 15) valid_length = 1;
            break;
        case card_types[1]:
            if (card.number.length == 16) valid_length = 1;
            break;
        case card_types[2]:
            if (card.number.length == 16) valid_length = 1;
            break;
        case card_types[3]:
            if (card.number.length == 13 || card.number.length == 16) valid_length = 1;
            break;
    }

    if (valid_length == 0) {
        return 0;
    } else {
        return validateLuhn(card.number);
    }
}


function validateLuhn(card_number) {
    // Convert string into an array
    var num_arr = (card_number.split(''));

    // 1. Starting with the next to last digit and continuing with every other digit going back to the beginning of the card, double the digit
    for (var i = num_arr.length - 2; i > -1; i -= 2) num_arr[i] = 2 * num_arr[i];

    // Convert array to string
    var num_str = (num_arr.toString()).replace(/,/g, '');

    // 2. Sum all doubled and untouched digits
    var total = eval(num_str.replace(/(\d)(?=\d)/g, '$1+'));

    // 3. If total is a multiple of 10, the number is valid
    var valid = total % 10 == 0 ? 1 : 0;

    return valid;
}


function printCards(cards) {
    cards.forEach(function (card) {
        var card_valid = card.valid == 1 ? '(valid)' : '(invalid)';
        console.log(card.card_type + ': ' + card.number + '\t' + card_valid);
    });
}